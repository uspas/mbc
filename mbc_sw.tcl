setws ./mbc/mbc.vitis
platform create -name {mbc_platform} -hw {./mbc/mbc.xsa}
platform write
domain create -name {standalone_microblaze_0} -display-name {standalone_microblaze_0} -os {standalone} -proc {microblaze_0} -runtime {cpp} -arch {32-bit} -support-app {hello_world}
platform generate
app create -name mbc_hello -platform mbc_platform -domain standalone_microblaze_0 -os standalone -lang c -template {Hello World}
app build -name mbc_hello
