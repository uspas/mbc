#connect -xvc-url localhost:2542
connect -url TCP:localhost:2542
targets -set -filter {name =~ "xc7k160t*"}
fpga ./mbc/mbc.runs/impl_1/mbc.bit
loadhw mbc/mbc.xsa
targets -set -filter {name =~ "MicroBlaze* #0"}
rst
dow ./mbc/mbc.vitis/mbc_hello/Debug/mbc_hello.elf
con
