create_project mbc ./mbc -part xc7k160tffg676-2 -force
create_bd_design "microblaze"
create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0
create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_0
create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.2 mdm_0
create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:11.0 microblaze_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 axi_intc_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0
create_bd_port -dir I -type rst ext_reset_in
connect_bd_net [get_bd_pins /proc_sys_reset_0/ext_reset_in] [get_bd_ports ext_reset_in]
create_bd_port -dir I -type clk -freq_hz 125000000 clk_125MHz
connect_bd_net [get_bd_pins /clk_wiz_0/clk_in1] [get_bd_ports clk_125MHz]
connect_bd_net [get_bd_pins proc_sys_reset_0/mb_debug_sys_rst] [get_bd_pins clk_wiz_0/reset]
connect_bd_net [get_bd_pins mdm_0/Debug_SYS_Rst] [get_bd_pins proc_sys_reset_0/mb_debug_sys_rst]
connect_bd_net [get_bd_pins clk_wiz_0/locked] [get_bd_pins proc_sys_reset_0/dcm_locked]
connect_bd_net [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins proc_sys_reset_0/slowest_sync_clk]
connect_bd_net [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins microblaze_0/Clk]
connect_bd_net [get_bd_pins axi_intc_0/s_axi_aclk] [get_bd_pins clk_wiz_0/clk_out1]
connect_bd_net [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins clk_wiz_0/clk_out1]
connect_bd_net [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins clk_wiz_0/clk_out1]
connect_bd_net [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins clk_wiz_0/clk_out1]
connect_bd_net [get_bd_pins axi_interconnect_0/M01_ACLK] [get_bd_pins clk_wiz_0/clk_out1]
connect_bd_net [get_bd_pins axi_uartlite_0/s_axi_aclk] [get_bd_pins clk_wiz_0/clk_out1]
connect_bd_intf_net [get_bd_intf_pins axi_intc_0/interrupt] [get_bd_intf_pins microblaze_0/INTERRUPT]
connect_bd_net [get_bd_pins axi_uartlite_0/interrupt] [get_bd_pins axi_intc_0/intr]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins axi_uartlite_0/S_AXI]
connect_bd_intf_net -boundary_type upper [get_bd_intf_pins axi_interconnect_0/M01_AXI] [get_bd_intf_pins axi_intc_0/s_axi]
connect_bd_net [get_bd_pins proc_sys_reset_0/mb_reset] [get_bd_pins microblaze_0/Reset]
connect_bd_net [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins axi_uartlite_0/s_axi_aresetn]
connect_bd_net [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins axi_interconnect_0/M01_ARESETN]
connect_bd_net [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins axi_interconnect_0/M00_ARESETN]
connect_bd_net [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins axi_interconnect_0/S00_ARESETN]
connect_bd_net [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins axi_interconnect_0/ARESETN]
connect_bd_net [get_bd_pins proc_sys_reset_0/peripheral_aresetn] [get_bd_pins axi_intc_0/s_axi_aresetn]
set_property -dict [list CONFIG.C_DATA_SIZE {32} CONFIG.C_D_AXI {1} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_DIV {1} CONFIG.C_ADDR_TAG_BITS {0} CONFIG.C_DCACHE_ADDR_TAG {0}] [get_bd_cells microblaze_0]
connect_bd_intf_net [get_bd_intf_pins microblaze_0/M_AXI_DP] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
assign_bd_address
validate_bd_design
regenerate_bd_layout
create_bd_cell -type hier microblaze_0_local_memory
create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 microblaze_0_local_memory/lmb_v10_0
create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 microblaze_0_local_memory/lmb_v10_1
create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 microblaze_0_local_memory/lmb_bram_if_cntlr_0
create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 microblaze_0_local_memory/lmb_bram_if_cntlr_1
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 microblaze_0_local_memory/blk_mem_gen_0
set_property -dict [list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.Enable_B {Use_ENB_Pin} CONFIG.Use_RSTB_Pin {true} CONFIG.Port_B_Clock {100} CONFIG.Port_B_Write_Rate {50} CONFIG.Port_B_Enable_Rate {100}] [get_bd_cells microblaze_0_local_memory/blk_mem_gen_0]
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/lmb_v10_1/LMB_Sl_0] [get_bd_intf_pins microblaze_0_local_memory/lmb_bram_if_cntlr_0/SLMB]
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/lmb_bram_if_cntlr_0/BRAM_PORT] [get_bd_intf_pins microblaze_0_local_memory/blk_mem_gen_0/BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/lmb_bram_if_cntlr_1/BRAM_PORT] [get_bd_intf_pins microblaze_0_local_memory/blk_mem_gen_0/BRAM_PORTB]
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/lmb_v10_0/LMB_Sl_0] [get_bd_intf_pins microblaze_0_local_memory/lmb_bram_if_cntlr_1/SLMB]
connect_bd_net [get_bd_pins microblaze_0_local_memory/lmb_v10_1/LMB_Clk] [get_bd_pins microblaze_0_local_memory/lmb_bram_if_cntlr_0/LMB_Clk]
connect_bd_net [get_bd_pins microblaze_0_local_memory/lmb_bram_if_cntlr_0/LMB_Clk] [get_bd_pins microblaze_0_local_memory/lmb_bram_if_cntlr_1/LMB_Clk]
connect_bd_net [get_bd_pins microblaze_0_local_memory/lmb_v10_0/LMB_Clk] [get_bd_pins microblaze_0_local_memory/lmb_v10_1/LMB_Clk]
connect_bd_net [get_bd_pins microblaze_0_local_memory/lmb_v10_1/SYS_Rst] [get_bd_pins microblaze_0_local_memory/lmb_v10_0/SYS_Rst]
connect_bd_net [get_bd_pins microblaze_0_local_memory/lmb_bram_if_cntlr_0/LMB_Rst] [get_bd_pins microblaze_0_local_memory/lmb_bram_if_cntlr_1/LMB_Rst]
connect_bd_net [get_bd_pins microblaze_0_local_memory/lmb_bram_if_cntlr_0/LMB_Rst] [get_bd_pins microblaze_0_local_memory/lmb_v10_1/SYS_Rst]
create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 microblaze_0_local_memory/DLMB
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/DLMB] [get_bd_intf_pins microblaze_0_local_memory/lmb_v10_1/LMB_M]
create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 microblaze_0_local_memory/ILMB
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/ILMB] [get_bd_intf_pins microblaze_0_local_memory/lmb_v10_0/LMB_M]
create_bd_pin -dir I microblaze_0_local_memory/LMB_Clk
connect_bd_net [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins microblaze_0_local_memory/lmb_v10_1/LMB_Clk]
create_bd_pin -dir I microblaze_0_local_memory/LMB_Rst
connect_bd_net [get_bd_pins microblaze_0_local_memory/LMB_Rst] [get_bd_pins microblaze_0_local_memory/lmb_v10_1/SYS_Rst]
connect_bd_intf_net [get_bd_intf_pins microblaze_0/DLMB] -boundary_type upper [get_bd_intf_pins microblaze_0_local_memory/DLMB]
connect_bd_intf_net [get_bd_intf_pins microblaze_0/ILMB] -boundary_type upper [get_bd_intf_pins microblaze_0_local_memory/ILMB]
connect_bd_net [get_bd_pins microblaze_0_local_memory/LMB_Rst] [get_bd_pins proc_sys_reset_0/bus_struct_reset]
set_property -dict [list CONFIG.C_MB_DBG_PORTS {2}] [get_bd_cells mdm_0]
connect_bd_intf_net [get_bd_intf_pins mdm_0/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
connect_bd_net [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins clk_wiz_0/clk_out1]
create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 uart_rtl_0
connect_bd_intf_net [get_bd_intf_pins axi_uartlite_0/UART] [get_bd_intf_ports uart_rtl_0]
set_property -dict [list CONFIG.C_BAUDRATE {115200}] [get_bd_cells axi_uartlite_0]
assign_bd_address
validate_bd_design
add_files -norecurse ./mbc.v
add_files -fileset constrs_1 -norecurse ./mbc.xdc
launch_runs impl_1 -to_step write_bitstream -jobs 10
wait_on_run impl_1
write_hw_platform -fixed -include_bit -force -file ./mbc/mbc.xsa
close_project
