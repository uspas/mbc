`timescale 1 ps / 1 ps

module mbc(input DDR_REF_CLK_P
,input DDR_REF_CLK_N
,output FPGA_RxD
,input FPGA_TxD
);


wire _ddr_ref_clk;
wire clk_125MHz;
IBUFGDS ibufgds_clk100(.I(DDR_REF_CLK_P),.IB(DDR_REF_CLK_N),.O(_ddr_ref_clk));
BUFG clk100_bufg(.I(_ddr_ref_clk),.O(clk_125MHz));
wire ext_reset_in =1'b1;

microblaze microblaze
(.clk_125MHz(clk_125MHz)
,.ext_reset_in(ext_reset_in)
,.uart_rtl_0_rxd(FPGA_TxD)
,.uart_rtl_0_txd(FPGA_RxD));

endmodule
