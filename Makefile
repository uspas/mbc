all: gw sw 
gw:
	vivado -mode batch -source mbc_gw.tcl
sw:
	xsct mbc_sw.tcl
run:
	xsct mbc_run.tcl

swclean:
	rm -rf mbc/mbc.vitis

clean: swclean
	rm  -rf mbc
